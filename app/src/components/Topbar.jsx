import React from 'react'
import Logo from "../images/logo.png"

export default function Topbar(props) {
    return (
        <div className="row shadow">
            <div className='container'>
                <img className="logo" src={Logo} />
            </div>
        </div>
    );
}