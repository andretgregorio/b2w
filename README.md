# Desafio Frontend - SkyHub - BIT SP

Para o teste, pedimos que seja entrega em até 5 dias, mas caso precise de mais tempo, nos avise que podemos negociar o prazo.

## Layout

O layout do desafio a ser desenvolvido está em /references/layout

Analogamente, os assets econtram-se em /references/assets

## Desafio
Desenvolver a página do layout, utilizando as seguintes tecnologias:

- HTML 5
- CSS 3
- JavaScript ES6/ES7
- React
- Redux
- Jest

### API
A API a ser consumida pela aplicação está descrita em http://challenge.skyhub.com.br/docs

Para iniciar o desenvolvimento, basta criar uma conta a partir da própria documentação:

- Operação [Nova Conta](http://challenge.skyhub.com.br/docs/#/Contas/accounts_create)
- Clique em "Try it out"
- Preencha seu nome
- E clique em "Execute"
- Use o *token* gerado para realizar as demais operações

#### Obrigatório:
- Código HTML semântico
- Consumir o arquivo os recursos da API disponibilizada
- Design Responsivo
- Testes automatizados

#### Desejável:
- Interações que enriqueçam a navegação pelo layout

Crie um *fork* desse repositório e nos envie um **pull request**.

Não esqueça de ensinar como instalamos e rodamos seu projeto em nosso ambiente. :sunglasses:

# Setando ambiente de dev
O projeto utiliza create-react-app como boilerplate. Para rodar em seu compurador, basta instalar as dependências de NPM com `npm install` (ou seu gerenciador de pacotes favorito). Com as dependências instaaladas, digite `npm start` para iniciar o servidor de desenvolvimento.

## Testes
Os testes com `npm test` são feitas com Jest e Puppeteer. O Puppeteer é uma biblioteca do Google que abre uma instância do chromium (que pode ser headless ou não). Com Chroomium é possível setar testes com um bot navegando pela página e avaliando se as configutações da pãgina estão corretas.